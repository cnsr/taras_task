from telegram.ext import CommandHandler, CallbackQueryHandler, Updater
from telegram import ChatAction, InlineKeyboardButton, InlineKeyboardMarkup, ParseMode, bot
import os
from dotenv import load_dotenv
from utils import *
from io import StringIO, BytesIO

# bot logging, mostly for debug
import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


# welcoming handler with description on how to use bot
def start(update, context):
    body = """
    Welcome to Currency Exchange bot!
    Use `/list` to get exchange data for USD.
    Use `/exchange AMOUNT CURRENCY_1 CURRENCY_2` to get how much is AMOUNT of CURRENCY_1 worth in CURRENCY_2
    e.g. `/exchange 100 USD UAH`
    (default currencies are USD to GBP)
    Use `/history CURRENCY_1/CURRENCY_2 7 days` to get graph of currencies relation for the data range
    """
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=body,
        parse_mode=ParseMode.MARKDOWN
    )


# list command handler
@typing
def get_list(update, context):
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=get_currency_data(),
        parse_mode=ParseMode.MARKDOWN
    )


# exchange command handler
@typing
def exchange(update, context):
    args = context.args
    print('ARGS', args)
    if 'to' in args:
        args.remove('to')
    if len(args) == 3:
        amount, out, to = args
        amount = float(amount)
        out = out.upper()
        to = to.upper()
        response = get_exchange_data(out, to, amount)
    elif len(args) == 2:
        # could fail if input is something like USD10
        out = args[0][0] # first char of '$10' string
        amount = float(args[0][1:])
        to = args[-1].upper()
        response = get_exchange_data(out, to, amount)
    elif len(args) == 0:
        response = get_exchange_data()
    else:
        response = 'Please, use `/exchange AMOUNT CURRENCY_1 CURRENCY_2` format.'
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=response,
        parse_mode=ParseMode.MARKDOWN
    )


# history command handler
@uploading
def history(update, context):
    args = context.args
    # remove useless parts of request params
    if 'for' in args:
        args.remove('for')
    try:
        # unpack values, process them
        pair, amount, period = args
        out, to = pair.split('/')
        amount = int(amount)
        response = get_history_data(out, to, amount, period)
        # send a plot picture
        context.bot.send_photo(
            chat_id=update.effective_chat.id,
            photo=open('out.png', 'rb')
        )
    except Exception as e:
        print(str(e))
        response = 'No exchange rate data is available for the selected currency'
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=response,
            parse_mode=ParseMode.MARKDOWN
        )



if __name__ == '__main__':
    load_dotenv()
    TOKEN = os.getenv("TOKEN")
    updater = Updater(token=TOKEN, use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('list', get_list))
    dispatcher.add_handler(CommandHandler('lst', get_list))
    dispatcher.add_handler(CommandHandler('exchange', exchange))
    dispatcher.add_handler(CommandHandler('help', start))
    dispatcher.add_handler(CommandHandler('history', history))
    updater.start_polling()
    updater.idle()
