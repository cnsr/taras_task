Create `.env` file, put token inside:
```sh
TOKEN='token12345'
```

Inside virtual environment, install all dependencies and run `python bot.py`.
