from functools import wraps
from telegram import ChatAction
import requests
import json
from pymongo import MongoClient
from datetime import datetime, timedelta
from symbols import symbols, avaliable_periods
import matplotlib.pyplot as plt

# database connection - mongo is overkill for such a project
db = MongoClient('localhost', 27017).currency_bot.data


# uploading status wrapper
def uploading(func):
  @wraps(func)
  def wrapped(update, context, *args, **kwargs):
    context.bot.send_chat_action(
      chat_id=update.effective_message.chat_id,
      action=ChatAction.UPLOAD_PHOTO)
    return func(update, context, *args, **kwargs)
  return wrapped


# handler for currency data args - only usd but theoretically any currency could be passed in if command args were checked in bot handler
def get_currency_data(currency='USD'):
    exists = db.find_one({'base': currency})
    now = datetime.now()
    # check whether 10 minutes have passed
    time_passed = now + timedelta(minutes=10) >= exists['date']
    if not exists or time_passed:
        # check whether in db
        re = requests.get(url=f"https://api.exchangeratesapi.io/latest?base={currency}")
        decoded = json.loads(re.text)
        # insert (update or create)
        insert(decoded)
        return make_list(decoded['rates'])
    else:
        print('already exists', exists)
        return make_list(exists['rates'])


# handler for exchange data args
def get_exchange_data(out='USD', to='GBP', amount=1):
    out = check_if_symbol(out)
    to = check_if_symbol(to)
    url = f'https://api.exchangeratesapi.io/latest?symbols={to}&base={out}'
    response = json.loads(requests.get(url).text)#['rates']
    if response.get('error', None):
        return response['error']
    response = amount * response['rates'][to]
    return f'{amount} {out} equals {response:.2f} {to}'


# handler for history data args
# i have no idea why would user input '7 days' into command, i'd use '/history usd cad' command
# typing '/' takes more time than a simple spacebar press
def get_history_data(out='USD', to='EUR', amount=7, period='days'):
    if amount == 0:
            amount = 1
    if period not in avaliable_periods:
        period = 'days'
    now = prepare_date(datetime.now())
    start = prepare_date(datetime.now() - timedelta(**{period:amount}))
    url = f"https://api.exchangeratesapi.io/history?start_at={start}&end_at={now}&base={to}&symbols={out}"
    response = json.loads(requests.get(url).text)
    if response.get('error', None):
        # return response['error']
        return "No exchange rate data is available for the selected currency"
    print(response)
    create_graph(response, out, to, start, now)
    return 'Done'

# wrapper to format data
def prepare_date(date):
    return date.strftime("%Y-%m-%d")

# check if input currency is a simbol, e.g. '$' - it will be translated into 'USD'
def check_if_symbol(curr):
    for pair in symbols:
        for x, y in pair.items():
            if x == curr:
                return y
    return curr

# typing status wrapper
def typing(func):
  @wraps(func)
  def wrapped(update, context, *args, **kwargs):
    context.bot.send_chat_action(
      chat_id=update.effective_message.chat_id,
      action=ChatAction.TYPING)
    return func(update, context, *args, **kwargs)
  return wrapped


# compile list into a string object
def make_list(data):
    response = '```\n'
    for (currency, price) in data.items():
        response += f'{currency}: {price:.2f}\n'
    response += '\n```'
    return response


# create or update database entry
def insert(data):
    if db.find_one({'base': data['base']}):
        # update
        data['date'] = datetime.now()
        db.update_one(
            {'base': data['base']},
            {'$set': data},
            upsert=False
        )
    else:
        # create
        data['date'] = datetime.now()
        db.insert(data)

def create_graph(data, out, to, start, finish):
    # unpack X and Y values - dates and amounts
    x_values = [date for date, values in data['rates'].items()]
    y_values = [float(values[out]) for date, values in data['rates'].items()]

    # build a generic plot
    plt.plot(x_values, y_values, label=f'{out} to {to}')

    # add labels to all plot points
    for x, y in zip(x_values, y_values):
        label = f'{y:.4f}'
        plt.annotate(
            label,
            (x,y),
            textcoords="offset points",
            xytext=(0,10),
            ha='center'
        )
    
    plt.xlabel(f'Dates')
    plt.ylabel(f'1 {out} in {to}')
    plt.title(f'{out} to {to} relation in dates from {start} to {finish}')
    plt.legend()
    # fix margins
    plt.tight_layout()
    x1, x2, y1, y2 = plt.axis()
    # 0.0005 could look even better
    plt.axis((x1, x2, y1 - 0.001, y2 + 0.001))
    plt.savefig('out.png')